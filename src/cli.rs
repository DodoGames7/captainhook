pub mod cli {
    use serde::{Deserialize, Serialize};
    use std::path::PathBuf;
    use structopt::StructOpt;

    #[derive(StructOpt, Debug)]
    #[structopt(name = env!("CARGO_PKG_NAME"), about = env!("CARGO_PKG_DESCRIPTION"))]
    pub struct Opts {
        #[structopt(short, long, help = "URL of the webhook", env = "CAPTAINHOOK_URL")]
        pub url: Option<String>,

        #[structopt(short, long, help = "Path to the profile")]
        pub profile: Option<PathBuf>,

        #[structopt(short, long, help = "Prints detailed output")]
        pub verbose: bool,

        #[structopt(short, long, help = "Prints output for debugging")]
        pub debug: bool,

        #[structopt(subcommand)]
        pub commands: Commands,
    }

    #[derive(StructOpt, Debug)]
    pub enum Commands {
        #[structopt(name = "send", about = "Sends the message")]
        Send {
            #[structopt(short, long, help = "Content of the message")]
            content: Option<String>,

            #[structopt(short, long, help = "Username of the webhook")]
            username: Option<String>,

            #[structopt(short, long, help = "URL to the avatar of the webhook")]
            avatar_url: Option<String>,

            #[structopt(short, long, help = "Voice the content of the message")]
            tts: bool,

            #[structopt(short, long, help = "Path to the file of the message")]
            file: Option<PathBuf>,
        },

        #[structopt(name = "edit", about = "Edits the existing message")]
        Edit {
            #[structopt(name = "ID", help = "ID of the message")]
            id: i64,

            #[structopt(name = "CONTENT", help = "New content of the message")]
            content: Vec<String>,
        },

        #[structopt(name = "delete", about = "Deletes the specified message")]
        Delete {
            #[structopt(name = "ID", help = "ID of the message")]
            id: i64,
        },
    }

    #[derive(Default, Serialize, Deserialize, Debug)]
    pub struct Profile {
        pub url: Option<String>,
        pub content: Option<String>,
        pub username: Option<String>,
        pub avatar_url: Option<String>,
        pub tts: Option<bool>,
        pub file: Option<PathBuf>,
    }
}
