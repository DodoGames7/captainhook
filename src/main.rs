mod cli;

use structopt::StructOpt;

use reqwest::blocking::multipart::{Form, Part};
use reqwest::blocking::{Client, Response};
use reqwest::header;

use serde_json::{json, Value};

use anyhow::{bail, Context, Result};

use cli::cli::*;

fn main() -> Result<()> {
    let opts = Opts::from_args();

    let profile: Profile = match opts.profile {
        Some(path) => toml::from_str(
            &std::fs::read_to_string(path).with_context(|| "Failed to read the file")?,
        )?,
        None => Profile::default(),
    };

    let url = match (opts.url, profile.url) {
        (Some(url), _) | (_, Some(url)) => url,
        _ => bail!("No webhook URL provided"),
    };

    let mut headers = header::HeaderMap::new();
    headers.insert(
        header::ACCEPT,
        header::HeaderValue::from_static("application/json"),
    );

    const USER_AGENT: &'static str =
        concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"));

    let client = Client::builder()
        .default_headers(headers)
        .user_agent(USER_AGENT)
        .build()?;

    let resp: Response = match opts.commands {
        Commands::Send {
            content,
            username,
            avatar_url,
            tts,
            file,
        } => client
            .post(url.clone())
            .multipart(
                Form::new()
                    .text(
                        "content",
                        content.unwrap_or(profile.content.unwrap_or_default()),
                    )
                    .text(
                        "username",
                        username.unwrap_or(profile.username.unwrap_or_default()),
                    )
                    .text(
                        "avatar_url",
                        avatar_url.unwrap_or(profile.avatar_url.unwrap_or_default()),
                    )
                    .text("tts", tts.to_string())
                    .part(
                        "file",
                        match (file, profile.file) {
                            (Some(file), _) | (_, Some(file)) => {
                                Part::file(file).with_context(|| "Failed to send the file")?
                            }
                            _ => Part::text("null"),
                        },
                    ),
            )
            .send()?,
        Commands::Edit { content, id } => client
            .patch(format!("{}/messages/{}", url, id))
            .json(&json!({ "content": content }))
            .send()?,
        Commands::Delete { id } => client.delete(format!("{}/messages/{}", url, id)).send()?,
    };

    let status = resp.status();

    let webhook_body: Value =
        serde_json::from_str(&client.get(url).send()?.text()?).unwrap_or_default();
    let resp_body: Value = serde_json::from_str(&resp.text()?).unwrap_or_default();

    if status.is_success() {
        if opts.verbose {
            println!(
                "Executed as {} in channel {} of guild {}",
                webhook_body["name"].as_str().unwrap(),
                webhook_body["channel_id"].as_str().unwrap(),
                webhook_body["guild_id"].as_str().unwrap(),
            )
        }
    } else {
        if opts.debug {
            bail!("{}\n{}\n{}", status, webhook_body, resp_body);
        } else {
            bail!(
                "{}",
                resp_body["message"]
                    .as_str()
                    .unwrap_or(status.canonical_reason().unwrap())
            );
        }
    }

    if opts.debug {
        println!("{}", status);
        println!("{}", webhook_body);
        println!("{}", resp_body);
    }

    Ok(())
}
