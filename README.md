# captainhook

captainhook is a command line front-end for operating Discord webhooks through your terminal. It can be used for sending, editing and deleting messages with custom usernames, avatars, embeds, and more, being a blazing fast CLI tool.

## Features

* Send, edit, delete messages
* Custom usernames and avatars, as well as TTS, embeds and files (WIP)
* Store webhook URL in an enviroment variable
* Profiles

## Usage

First, you need to obtain Webhook URL. You can do that by going to Server Settings -> Integrations -> Webhooks and clicking on the desired webhook or creating a new one. Once you've done that, open your beloved terminal and start tinkering:

```
captainhook --url https://discord.com/api/webhooks/XXXXX/XXXXX send --content "Hello world"
```

Please note that you have to put any text with spaces or other special symbols into quotes. The same goes for webhook's username, for instance:

```
captainhook --url https://discord.com/api/webhooks/XXXXX/XXXXX send --content "Hello world" --username "Captain Hook"
```

In order to mention a member, channel or role, you have to use the special markup:

```
captainhook --url https://discord.com/api/webhooks/XXXXX/XXXXX send "What's up, <@ID>?"
```

```
captainhook --url https://discord.com/api/webhooks/XXXXX/XXXXX send "Greatest channel ever <#ID>"
```

Tired of clogging up your beautiful terminal? Put the URL in an environment variable

```
export CAPTAINHOOK_URL="https://discord.com/api/webhooks/XXXXX/XXXXX"
```

...or a TOML file!

```toml
url = "https://discord.com/api/webhooks/XXXXX/XXXXX"
```

In case of enviroment variable, you can simply omit `--url`. If you want to load data from a file, use `--profile /path/to/profile.toml`. You can store `content` or any other properties as well:
```toml
url = "https://discord.com/api/webhooks/XXXXX/XXXXX"
content = "I'm the rustiest"
username = "Rust"
avatar-url = "https://www.rust-lang.org/logos/rust-logo-128x128.png"
```

You can also override the properties if needed:
```
captainhook --profile profile.toml --content "I'm *not* the rustiest"
```
